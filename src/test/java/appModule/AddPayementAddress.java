package appModule;

import org.openqa.selenium.WebDriver;

import pageObject.Delivery;
import utility.Data;
import utility.RandomString;

public class AddPayementAddress {
	
	public static void AddAddress(WebDriver driver) throws Exception{

		try {
			Delivery.AdressName(driver).sendKeys(RandomString.NameString());
			Delivery.CompanyName(driver).sendKeys(Data.Company);
			Delivery.NVoie(driver).sendKeys(Data.Adresse);
			Delivery.PostalCode(driver).sendKeys(Data.PostalCode_FR);
			Delivery.City(driver).sendKeys(Data.City);
			Thread.sleep(1000);
			Delivery.SubmitAddress(driver).click();
			Delivery.ReturnToDelivery_btn(driver).click();
		}catch(Exception e) {
			System.out.println("erreur : création d'adresse de facturation.");
			throw(e);
		}
		
		
	}

}

