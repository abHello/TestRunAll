package utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.Assert;


public class CompareHours {
	
	
	public static void Compare(String time1, String time2) throws Exception{
		
		if(!(time2.contains(":")) || (time2.length()>5)) {
			time2="00:00";
		}
		
		SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
		Date time11 = parser.parse(time1);
		Date time22 = parser.parse(time2);
		
		try {
			if (time11.after(time22) ) {
				Assert.assertFalse(true);
			}else {
				Assert.assertTrue(true);
			}    
		} catch (Exception e) {
		   throw(e); 
		}
	
	}
}




