package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ObligatoryInformation {
	public static WebElement element=null; 

	public static WebElement Obligatory_Mail(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_Email']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_Mail n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Incorrect_Mail(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_Email' and @data-fv-validator='regexp']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Incorrect_Mail n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
	
	}
	
	
	public static WebElement Obligatory_Password(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_Password']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_Password n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Incorrect_Password(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_Password' and @data-fv-validator='regexp']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Incorrect_Mail n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_Company(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_CompanyName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_Company n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_Title(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_Title']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_Title n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_FirstName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_FirstName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_FirstName n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_LastName(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_LastName']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_LastName n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_PhoneHome(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_PhoneHome']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_PhoneHome n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_Mobile(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_Mobile']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_Mobile n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_Adresse1(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_Address1']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_Adresse1 n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_PostalCode(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-validator='notEmpty' and @data-fv-for='RegistrationForm_PostalCode']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_PostalCode n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement PostalCode_Incorrect(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-validator='regexp' and @data-fv-for='RegistrationForm_PostalCode']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_PostalCode n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement Obligatory_City(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//small[@data-fv-for='RegistrationForm_City']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Obligatory_City n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}
	
	
	public static WebElement IncorrectPhone(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='col-xs-6']/div/small[2]"));
		
		}catch(Exception e) {
			System.out.println("L'élément IncorrectPhone n'a pas été trouvé dans la page Registration_Form.");
			throw(e);
		}
		return element;
		
	}

}
