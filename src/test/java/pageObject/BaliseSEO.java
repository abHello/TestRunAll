package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BaliseSEO {
	
public static WebElement element=null;
	
	public static WebElement Univer_h1(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='block__titlebdr product']/h1"));
		
		}catch(Exception e) {
			System.out.println("L'élément Univer_h1 n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Univer_h2(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='col-xs-8']/h2"));
		
		}catch(Exception e) {
			System.out.println("L'élément Univer_h2 n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement PDT_h1(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//h1[@class='pv__name block__titlebdr']"));
		
		}catch(Exception e) {
			System.out.println("L'élément PDT_h1 n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement PDT_h2(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//h2[@class='pv__features--title']"));
		
		}catch(Exception e) {
			System.out.println("L'élément PDT_h2 n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement meta_Description(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("/html/head/meta[4]"));
		
		}catch(Exception e) {
			System.out.println("L'élément meta_Description n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}


}
