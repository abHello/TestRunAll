package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Home {

public static WebElement element=null;
	
	public static WebElement BigMenu_lst(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//ul[@class='navigation']"));
		
		}catch(Exception e) {
			System.out.println("L'élément BigMenu n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
		
	
	
	public static WebElement Logo_img(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//img[@src='/INTERSHOP/static/WFS/RAJA-FR-Site/-/RAJA-FR/fr_FR/assets/images/header/logo/logo_fr_FR.svg']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Logo n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Navigation(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='swiper-tabs swiper-container inst-0']")); 
		
		}catch(Exception e) {
			System.out.println("L'élément navigation n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement SearchTerm(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//input[@id='searchTerm_Header']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Moteur de recherche n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement MyAccount_btn(WebDriver driver) throws Exception {
		
		try {
			Thread.sleep(1000);
		element=driver.findElement(By.xpath("//div[@class='quickaccess quickaccess__account-noconnected']")); 
		
		}catch(Exception e) {
			System.out.println("L'élément Mon compte n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Basket_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='quickaccess__btn device']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Panier n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement Univers_menu(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='menu__univers']"));
		
		}catch(Exception e) {
			System.out.println("L'élément menu_univers n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
		}
	
	
	public static WebElement MyAccount_Connected(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='quickaccess quickaccess__account']"));
		
		}catch(Exception e) {
			System.out.println("L'élément MyAccount_Connected n'a pas été trouvé dans la Home.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement See_Profil_btn(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='account__content']/ul/li"));
		
		}catch(Exception e) {
			System.out.println("L'élément See_Profil_btn n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement OrderQA(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='quickaccess quickaccess__reference']"));
		
		}catch(Exception e) {
			System.out.println("L'élément OrderQA n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement ContactUS(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='footer__links--inner']/div[4]"));
		
		}catch(Exception e) {
			System.out.println("L'élément ContactUS n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement LandingPage(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//div[@class='col-xs-12']/div[2]/div"));
		
		}catch(Exception e) {
			System.out.println("L'élément OrderQA n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Robots(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//pre[@style='word-wrap: break-word; white-space: pre-wrap;']"));
		
		}catch(Exception e) {
			System.out.println("L'élément Robots n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement Product_sitemap(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"collapsible2\"]"));
		
		}catch(Exception e) {
			System.out.println("L'élément Product_sitemap n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement CatalogueCategory_sitemap(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"collapsible4\"]"));
		
		}catch(Exception e) {
			System.out.println("L'élément CatalogueCategory_sitemap n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
	public static WebElement StaticPage_sitemap(WebDriver driver) throws Exception {
		
		try {
		element=driver.findElement(By.xpath("//*[@id=\"collapsible6\"]"));
		
		}catch(Exception e) {
			System.out.println("L'élément StaticPage_sitemap n'a pas été trouvé dans la Home_Page.");
			throw(e);
		}
		return element;
	}
	
	
}
